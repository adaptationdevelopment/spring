package br.com.adaptation.core.spring.web.servlet.config;

import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.context.request.WebRequestInterceptor;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.config.annotation.InterceptorRegistration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import br.com.adaptation.core.spring.annotation.Interceptor;

@Configuration
public class InterceptorConfigurer extends WebMvcConfigurerAdapter {

	private Logger logger = Logger.getLogger(InterceptorConfigurer.class);

	@Autowired
	ApplicationContext appContext;

	@Override
	public void addInterceptors(InterceptorRegistry registry) {

		Map<String, Object> interceptors = appContext
				.getBeansWithAnnotation(Interceptor.class);

		if (!interceptors.isEmpty()) {
			Iterator<Entry<String, Object>> iterator = interceptors.entrySet()
					.iterator();

			while (iterator.hasNext()) {

				Map.Entry mapEntry = (Map.Entry) iterator.next();
				Object object = mapEntry.getValue();
				
				if (object instanceof HandlerInterceptor) {

					logger.info("Add Handler Interceptor [ "
							+ object.getClass() + " ]..");
					HandlerInterceptor interceptor = (HandlerInterceptor) object;
					applyPathPatterns(registry
							.addInterceptor(interceptor), object);
					
				} else if (object instanceof WebRequestInterceptor) {
					
					logger.info("Add WebRequest Interceptor [ "
							+ object.getClass() + " ]..");
					
					WebRequestInterceptor interceptor = (WebRequestInterceptor) object;
					
					applyPathPatterns(registry
							.addWebRequestInterceptor(interceptor), object);
					
				}
				
			}
		}
	}
	
	public void applyPathPatterns(InterceptorRegistration registration, Object object) {
		
		Interceptor interceptor = object.getClass().getAnnotation(Interceptor.class);
		
		String add = interceptor.addPathPatterns();
		if (!add.isEmpty()) {
			logger.debug("Added Path Patterns [ " + add + " ] on interceptor [ " + object.getClass() + " ]..");
			registration.addPathPatterns(add);
		}
		
		String exclude = interceptor.excludePathPatterns();
		if (!exclude.isEmpty()) {
			logger.debug("Excluded Path Patterns [ " + exclude + " ] on interceptor [ " + object.getClass() + " ]..");
			registration.excludePathPatterns(exclude);
		}
		
	}
}