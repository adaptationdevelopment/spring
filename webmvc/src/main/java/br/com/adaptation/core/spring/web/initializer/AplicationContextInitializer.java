package br.com.adaptation.core.spring.web.initializer;

import br.com.adaptation.core.spring.config.ApplicationConfiguration;
import lombok.extern.log4j.Log4j;
import org.springframework.web.filter.CharacterEncodingFilter;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

import javax.servlet.Filter;
import javax.servlet.ServletRegistration;

@Log4j
public class AplicationContextInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {

    /**
     * {@link org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer#getRootConfigClasses()}
     */
    @Override
    protected Class<?>[] getRootConfigClasses() {
        return new Class<?>[]{ApplicationConfiguration.class};
    }

    /**
     * {@link org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer#getServletConfigClasses()}
     */
    @Override
    protected Class<?>[] getServletConfigClasses() {
        return null;
    }

    /**
     * {@link org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer#getServletMappings()}
     */
    @Override
    protected String[] getServletMappings() {
        return new String[]{"/"};
    }

    /**
     * {@link org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer#getServletFilters()}
     */
    @Override
    protected Filter[] getServletFilters() {

        log.info("Registering Servlet Filters ...");

        // Encoding
        CharacterEncodingFilter characterEncodingFilter = new CharacterEncodingFilter();
        characterEncodingFilter.setEncoding("UTF-8");

        return new Filter[]{characterEncodingFilter};
    }

    /**
     * {@link org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer#customizeRegistration(javax.servlet.ServletRegistration.Dynamic)}
     */
    @Override
    protected void customizeRegistration(ServletRegistration.Dynamic reg) {
        log.info("Customizing Servlet ...");
        reg.setInitParameter("throwExceptionIfNoHandlerFound", "true");
    }

    /*
    @Override
    public void onStartup(ServletContext context) throws ServletException {

        log.info("Starting Spring Web Application..");

        AnnotationConfigWebApplicationContext rootContext = new AnnotationConfigWebApplicationContext();

        ConfigurableEnvironment env = rootContext.getEnvironment();

        for (String profile : env.getActiveProfiles()) {
            log.info("Active Profile: " + profile);
        }
        rootContext.register(ApplicationConfiguration.class);

        context.addListener(new ContextLoaderListener(rootContext));

        addServiceDispatcherServlet(context, rootContext);
    }


    private void addServiceDispatcherServlet(ServletContext context, AnnotationConfigWebApplicationContext rootContext) {

        final String SERVICES_MAPPING = "/";

        DispatcherServlet ds = new DispatcherServlet(rootContext);
        ds.setThrowExceptionIfNoHandlerFound(true);

        ServletRegistration.Dynamic dispatcher = context.addServlet("servicesDispatcher", new DispatcherServlet(rootContext));
        dispatcher.setLoadOnStartup(1);
        Set<String> mappingConflicts = dispatcher.addMapping(SERVICES_MAPPING);

        if (!mappingConflicts.isEmpty()) {
            for (String s : mappingConflicts) {
                log.error("Mapping conflict: " + s);
            }
            throw new IllegalStateException("'ServicesDispatcher' could not be mapped to '" + SERVICES_MAPPING + "'");
        }
    }
    */

}