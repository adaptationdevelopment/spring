package br.com.adaptation.core.spring.config;

import javax.naming.NamingException;
import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jndi.JndiObjectFactoryBean;

@Configuration
public class DataSourceConfiguration {

	private Logger logger = Logger.getLogger(DataSourceConfiguration.class);

	@Value("${jndi.name}")
	private String name;
	
	@Bean
	public DataSource dataSource(){
		logger.info("Creating Jndi DataSource Bean..");
		JndiObjectFactoryBean jofb = new JndiObjectFactoryBean();
		jofb.setJndiName(name);
		//jndiObjectFactoryBean.setResourceRef(true);
		try {
			jofb.afterPropertiesSet();
		} catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (NamingException e) {
			e.printStackTrace();
		}
		return (DataSource) jofb.getObject();
	}

}