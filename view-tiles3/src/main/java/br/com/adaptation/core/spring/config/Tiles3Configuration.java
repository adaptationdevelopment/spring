package br.com.adaptation.core.spring.config;

import org.apache.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.view.UrlBasedViewResolver;
import org.springframework.web.servlet.view.tiles3.TilesConfigurer;
import org.springframework.web.servlet.view.tiles3.TilesView;

@Configuration
//@PropertySource("classpath:tiles3.properties")
public class Tiles3Configuration {

	// Log4j
	private Logger logger = Logger.getLogger(Tiles3Configuration.class);

	@Bean
	public UrlBasedViewResolver tilesViewResolver() {
		logger.info("Creating Tiles3 View Resolver Bean..");
		
		UrlBasedViewResolver resolver = new UrlBasedViewResolver();
		resolver.setViewClass(TilesView.class);
		resolver.setOrder(0);
		
		return resolver;
		
	}
	
	@Bean
	public TilesConfigurer tilesConfigurer() {
		logger.info("Creating Tiles3 Configurer..");
		
		TilesConfigurer tc = new TilesConfigurer();
		return tc;
	}

}