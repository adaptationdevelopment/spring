package br.com.adaptation.core.spring.config;

import org.apache.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;

@Configuration
public class RedisConfiguration {

	// Log4j
	private Logger logger = Logger.getLogger(RedisConfiguration.class);

	@Bean
	public RedisTemplate<Object, Object> redisTemplate(RedisConnectionFactory redisConnectionFactory) {
		logger.info("Create Redis Template Bean..");
		RedisTemplate<Object, Object> redisTemplate = new RedisTemplate<Object, Object>();
		redisTemplate.setConnectionFactory(redisConnectionFactory);
		return redisTemplate;
	}
	
	/*
	@Bean
	public RedisAtomicLong redisAtomicLong(RedisConnectionFactory redisConnectionFactory) {
		return new RedisAtomicLong("instituicao", redisConnectionFactory);
	}
	*/

	/*
	@Bean
	public RedisSerializer<Object> valueSerializer() {
		return new JacksonJsonRedisSerializer<Object>(Object.class);
	}
	*/
	
}
