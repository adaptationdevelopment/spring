package br.com.adaptation.core.spring.config;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

@Configuration
public class JspConfiguration{

	private Logger logger = Logger.getLogger(JspConfiguration.class);

	@Value("${jsp.prefix:/WEB-INF/views/}")
	private String prefix;
	
	@Value("${jsp.suffix:.jsp}")
	private String suffix;
	
	@Bean
	public InternalResourceViewResolver configureInternalResourceViewResolver()
	{
		logger.info("Creating InternalResourceViewResolver Bean..");
		
		InternalResourceViewResolver resolver = new InternalResourceViewResolver();
		resolver.setPrefix("/WEB-INF/views/");
		resolver.setSuffix(".jsp");
		resolver.setOrder(1);
		return resolver;
	}

}