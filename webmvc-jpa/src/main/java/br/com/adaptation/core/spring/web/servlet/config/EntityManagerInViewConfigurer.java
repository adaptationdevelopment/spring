package br.com.adaptation.core.spring.web.servlet.config;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.support.OpenEntityManagerInViewInterceptor;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
public class EntityManagerInViewConfigurer extends WebMvcConfigurerAdapter {

	// Log4j
	private Logger logger = Logger.getLogger(EntityManagerInViewConfigurer.class);

	@Autowired
	LocalContainerEntityManagerFactoryBean entityManagerFactory;
	
	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		logger.info("Adding OpenEntityManagerInView Interceptor..");	
		OpenEntityManagerInViewInterceptor interceptor = new OpenEntityManagerInViewInterceptor();
		interceptor.setEntityManagerFactory(entityManagerFactory.getObject());
		registry.addWebRequestInterceptor(interceptor);
	}

}