package br.com.adaptation.core.spring.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

@Configuration
@ImportResource({"classpath:spring-security-cas.xml"})
public class JasigCASConfiguration {

}
