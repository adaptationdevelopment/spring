package br.com.adaptation.core.spring.web;

import javax.servlet.FilterRegistration;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;

import org.apache.log4j.Logger;
import org.jasig.cas.client.session.SingleSignOutFilter;
import org.jasig.cas.client.session.SingleSignOutHttpSessionListener;
import org.springframework.web.WebApplicationInitializer;

public class JasigCASInitializer implements WebApplicationInitializer {

	// Log4j
	private Logger logger = Logger.getLogger(JasigCASInitializer.class);

	@Override
	public void onStartup(ServletContext context) throws ServletException {

		logger.info("Starting Jasig CAS Authentication..");

		FilterRegistration.Dynamic filterSignOut = context
				.addFilter("CASSingleSignOutFilter", SingleSignOutFilter.class);
		filterSignOut.addMappingForUrlPatterns(null, false, "/*");
		
		/*
		FilterRegistration.Dynamic filterAuth = context
				.addFilter("AuthenticationFilter", AuthenticationFilter.class);
		filterAuth.setInitParameter("casServerLoginUrl", "https://10.0.35.8:8443/cas-server/login");
		filterAuth.addMappingForUrlPatterns(null, false, "/*");
		
		FilterRegistration.Dynamic ticketValidation = context
				.addFilter("TicketValidationFilter", Cas20ProxyReceivingTicketValidationFilter.class);
		ticketValidation.setInitParameter("casServerUrlPrefix", "https://10.0.35.8:8443/cas-server");
		ticketValidation.addMappingForUrlPatterns(null, false, "/*");
		*/
		
		context.addListener(new SingleSignOutHttpSessionListener());

	}
}