package br.com.adaptation.core.spring.config;

import java.beans.PropertyVetoException;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.mchange.v2.c3p0.ComboPooledDataSource;

@Configuration
public class DataSourceConfiguration {

	private Logger logger = Logger.getLogger(DataSourceConfiguration.class);

	@Value("${db.driver}")
	private String driver;
	
	@Value("${db.url}")
	private String url;
	
	@Value("${db.username}")
	private String username;
	
	@Value("${db.password}")
	private String password;
	
	@Value("${c3p0.min_pool_size:5}")
	private Integer minPoolSize;
	
	@Value("${c3p0.max_pool_size:30}")
	private Integer maxPoolSize;
	
	@Value("${c3p0.max_statements:50}")
	private Integer maxStatements;
	
	@Value("${c3p0.login_timeout:180}")
	private Integer loginTimeout;
	
	@Value("${c3p0.idle_connection_test_period:100}")
	private Integer idleConnectionTestPeriod;
	
	@Bean
	public DataSource dataSource() throws PropertyVetoException, SQLException {
		logger.info("Creating C3P0 DataSource Bean..");
		
		ComboPooledDataSource ds = new ComboPooledDataSource();
		
		ds.setDriverClass(driver);
		ds.setJdbcUrl(url);
		ds.setUser(username);
		ds.setPassword(password);
	
		ds.setMinPoolSize(minPoolSize);
		ds.setMaxPoolSize(maxPoolSize);
		ds.setMaxStatements(maxStatements);
		ds.setLoginTimeout(loginTimeout);
		ds.setIdleConnectionTestPeriod(idleConnectionTestPeriod);
		
		return ds;
	}

}