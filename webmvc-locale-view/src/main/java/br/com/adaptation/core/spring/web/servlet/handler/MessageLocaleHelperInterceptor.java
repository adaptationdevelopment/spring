package br.com.adaptation.core.spring.web.servlet.handler;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import br.com.adaptation.core.spring.annotation.Interceptor;
import br.com.adaptation.core.spring.helper.MessageLocaleHelper;
import br.com.adaptation.core.spring.helper.MessageLocaleModel;

@Interceptor
public class MessageLocaleHelperInterceptor extends HandlerInterceptorAdapter {

	@Autowired
	MessageSource messageSource;
	
	@Autowired
	MessageLocaleHelper messageLocaleHelper;
	
	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response,
			Object handler, ModelAndView modelAndView) throws Exception {
		
		List<MessageLocaleModel> messages = messageLocaleHelper.getMessages();
		
		if (messages.size()>0) {
			
			for (MessageLocaleModel message : messages) {
				String m = messageSource.getMessage(message.getValue(), message.getParams(), request.getLocale());
				Map<String, Object> model = modelAndView.getModel();
				model.put(message.getKey().toString().toLowerCase(), m);
			}
		
		}
		
		
	}

}
