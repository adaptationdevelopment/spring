package br.com.adaptation.core.spring.web.servlet.config;

import lombok.extern.log4j.Log4j;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ContentNegotiationConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
@Log4j
public class WebMvcViewConfigurer extends WebMvcConfigurerAdapter {

    @Override
    public void configureContentNegotiation(
            ContentNegotiationConfigurer configurer) {

        log.info("Setting Content Negotiation Configurer ..");

        configurer.favorPathExtension(true).ignoreAcceptHeader(false)
                .useJaf(false);
    }

    /*
    @Override
    public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {
        configurer.enable();
    }
    */

}