package br.com.adaptation.core.spring.web.servlet.config;

import org.apache.log4j.Logger;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
public class ResourceHandlersConfigurer extends WebMvcConfigurerAdapter {

	private Logger logger = Logger.getLogger(ResourceHandlersConfigurer.class);

	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		logger.info("Add Resources Handlers ..");
		registry.addResourceHandler("/static/**").addResourceLocations(
				"/static/", "classpath:static/");
	}



}