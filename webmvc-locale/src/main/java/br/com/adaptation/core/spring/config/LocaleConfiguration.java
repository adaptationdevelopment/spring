package br.com.adaptation.core.spring.config;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.core.type.filter.AnnotationTypeFilter;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

import br.com.adaptation.core.spring.annotation.LocaleMessageSource;
import br.com.adaptation.core.spring.helper.MessageLocaleHelper;

@Configuration
public class LocaleConfiguration {

	private Logger logger = Logger.getLogger(LocaleConfiguration.class);

	@Value("${spring.base_packages}")
	private String basePackages;

	@Value("${locale.default:pt_BR}")
	private String lang;

	@Value("${locale.param:lang}")
	private String param;

	@Value("${locale.base_name}")
	private String baseName;

	@Value("${locale.encoding:UTF-8}")
	private String encoding;

	@Bean
	public LocaleResolver localeResolver() {
		logger.info("Setting Default Locale ..");
		SessionLocaleResolver slr = new SessionLocaleResolver();
		slr.setDefaultLocale(new Locale(lang));
		return slr;
	}

	@Bean
	public LocaleChangeInterceptor localeChangeInterceptor() {
		logger.info("Setting Interceptor Param ..");
		LocaleChangeInterceptor lci = new LocaleChangeInterceptor();
		lci.setParamName(param);
		return lci;
	}

	@Bean
	public MessageSource messageSource() throws ClassNotFoundException {
		logger.info("Setting Message Source ..");

		ClassPathScanningCandidateComponentProvider scanner = new ClassPathScanningCandidateComponentProvider(
				false);
		scanner.addIncludeFilter(new AnnotationTypeFilter(
				LocaleMessageSource.class));

		List<String> baseNamesList = new ArrayList<String>();
		baseNamesList.add(baseName);

		for (BeanDefinition candidate : scanner
				.findCandidateComponents(basePackages)) {

			String beanClassName = candidate.getBeanClassName();

			String baseNameDef = Thread.currentThread().getContextClassLoader()
					.loadClass(beanClassName)
					.getAnnotation(LocaleMessageSource.class).basename();

			logger.info("Found l18n classpath file [" + baseNameDef + "]");

			baseNamesList.add("classpath:" + baseNameDef);
		}

		String[] baseNames = baseNamesList.toArray(new String[baseNamesList
				.size()]);

		ReloadableResourceBundleMessageSource msgSource = new ReloadableResourceBundleMessageSource();
		msgSource.setBasenames(baseNames);
		msgSource.setDefaultEncoding(encoding);
		return msgSource;
	}
	
	@Bean
	public MessageLocaleHelper messageLocaleHelper() {
		return new MessageLocaleHelper();
	}

}
