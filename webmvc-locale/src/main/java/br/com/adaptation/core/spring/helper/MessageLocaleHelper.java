package br.com.adaptation.core.spring.helper;

import java.util.ArrayList;
import java.util.List;

public class MessageLocaleHelper {

	private List<MessageLocaleModel> messages;

	public MessageLocaleHelper() {
		messages = new ArrayList<MessageLocaleModel>();
	}

	protected void add(MessageLocaleType key, String value) {
		this.messages.add(new MessageLocaleModel(key, value));
	}

	protected void add(MessageLocaleType key, String value, String[] params) {
		this.messages.add(new MessageLocaleModel(key, value, params));
	}

	public void success(String value) {
		this.add(MessageLocaleType.SUCCESS, value);
	}

	public void success(String value, String[] params) {
		this.add(MessageLocaleType.SUCCESS, value, params);
	}

	public void info(String value) {
		this.add(MessageLocaleType.INFO, value);
	}

	public void info(String value, String[] params) {
		this.add(MessageLocaleType.INFO, value, params);
	}

	public void warning(String value) {
		this.add(MessageLocaleType.WARNING, value);
	}

	public void warning(String value, String[] params) {
		this.add(MessageLocaleType.WARNING, value, params);
	}
	
	public void error(String value) {
		this.add(MessageLocaleType.ERROR, value);
	}

	public void error(String value, String[] params) {
		this.add(MessageLocaleType.ERROR, value, params);
	}

	public List<MessageLocaleModel> getMessages() {
		return this.messages;
	}

}
