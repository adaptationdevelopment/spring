package br.com.adaptation.core.spring.helper;

public enum MessageLocaleType {
	WARNING, ERROR, SUCCESS, INFO
}
