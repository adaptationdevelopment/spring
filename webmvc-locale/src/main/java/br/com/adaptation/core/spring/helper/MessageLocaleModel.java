package br.com.adaptation.core.spring.helper;

public class MessageLocaleModel {

	private MessageLocaleType key;
	private String value;
	private String[] params;
	
	public MessageLocaleModel(MessageLocaleType key, String value) {
		this.key = key;
		this.value = value;
		params = null;
	}
	
	public MessageLocaleModel(MessageLocaleType key, String value, String[] params) {
		this.key = key;
		this.value = value;
		this.params = params;
	}
	
	public MessageLocaleType getKey() {
		return key;
	}
	
	public String getValue() {
		return value;
	}
	
	public String[] getParams() {
		return params;
	}
	
}