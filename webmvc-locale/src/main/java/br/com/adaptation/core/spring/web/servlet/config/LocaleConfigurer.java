package br.com.adaptation.core.spring.web.servlet.config;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;

@Configuration
public class LocaleConfigurer extends WebMvcConfigurerAdapter {

	private Logger logger = Logger.getLogger(LocaleConfigurer.class);

	@Autowired
	LocaleChangeInterceptor localeChangeInterceptor;
	
	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		logger.info("Adding locale Change Interceptor ..");
		registry.addInterceptor(localeChangeInterceptor);
	}

}
