package br.com.adaptation.core.spring.config;

import lombok.extern.log4j.Log4j;
import org.hibernate.jpa.HibernatePersistenceProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.PropertiesFactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;

import javax.persistence.spi.PersistenceProvider;
import java.util.Properties;

@Configuration
@Log4j
public class JpaHibernateConfiguration {

	@Autowired
	private Environment env;

    private static final String PROPERTY_HIBERNATE_DIALECT = "hibernate.dialect";

    private static final String PROPERTY_HIBERNATE_SHOW_SQL = "hibernate.show_sql";

    private static final String PROPERTY_HIBERNATE_HBM2DDL_IMPORT_FILES = "hibernate.hbm2ddl.import_files";

    private static final String PROPERTY_HIBERNATE_HBM2DDL_AUTO = "hibernate.hbm2ddl.auto";

    private static final String PROPERTY_HIBERNATE_FORMAT_SQL = "hibernate.format_sql";

    private static final String PROPERTY_GENERATE_STATISTICS = "hibernate.generate_statistics";

    private static final String PROPERTY_CACHE_USE_SECOND_LEVEL_CACHE = "hibernate.cache.use_second_level_cache";

    private static final String PROPERTY_CACHE_USE_QUERY_CACHE = "hibernate.cache.use_query_cache";

    private static final String PROPERTY_CACHE_USE_STRUCTURED_ENTRIES = "hibernate.cache.use_structured_entries";

    private static final String PROPERTY_SEARCH_DEFAULT_INDEXBASE = "hibernate.search.default.indexBase";

    @Bean
	public PersistenceProvider persistenceProvider() {
		log.info("Creating Hibernate Persistence Provider Bean..");
        return new HibernatePersistenceProvider();
	}
	
	@Bean
	public JpaVendorAdapter jpaAdapter() {
		log.info("Creating Hibernate Jpa Adapter Bean..");
		HibernateJpaVendorAdapter jpaAdapter = new HibernateJpaVendorAdapter();
		return jpaAdapter;
	}

    @Bean
    public PropertiesFactoryBean jpaProperties()
    {
        PropertiesFactoryBean propertiesFactoryBean = new PropertiesFactoryBean();

        Properties properties = new Properties();

        putProperties(properties, PROPERTY_HIBERNATE_DIALECT, env);
        putProperties(properties, PROPERTY_HIBERNATE_SHOW_SQL, env);
        putProperties(properties, PROPERTY_HIBERNATE_HBM2DDL_AUTO, env);
        putProperties(properties, PROPERTY_HIBERNATE_HBM2DDL_IMPORT_FILES, env);
        putProperties(properties, PROPERTY_HIBERNATE_FORMAT_SQL, env);
        putProperties(properties, PROPERTY_GENERATE_STATISTICS, env);
        putProperties(properties, PROPERTY_CACHE_USE_SECOND_LEVEL_CACHE, env);
        putProperties(properties, PROPERTY_CACHE_USE_QUERY_CACHE, env);
        putProperties(properties, PROPERTY_CACHE_USE_STRUCTURED_ENTRIES, env);
        putProperties(properties, PROPERTY_SEARCH_DEFAULT_INDEXBASE, env);

        propertiesFactoryBean.setProperties(properties);

        return propertiesFactoryBean;
    }

    private static void putProperties(Properties properties, String key, Environment env){

        String value = env.getProperty(key);
        if(value != null){
            properties.setProperty(key, value);
        }
    }


}