package br.com.adaptation.core.spring.config;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.PropertiesFactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;

import javax.persistence.spi.PersistenceProvider;
import javax.sql.DataSource;
import java.io.IOException;

@Configuration
public class JpaConfiguration {

    private Logger logger = Logger.getLogger(JpaConfiguration.class);

    @Value("${jpa.packages_to_scan}")
    private String packagesToScan;

    @Autowired
    private JpaVendorAdapter jpaAdapter;

    @Autowired
    private PersistenceProvider persistenceProvider;

    @Autowired
    private DataSource dataSource;

    @Autowired
    private PropertiesFactoryBean jpaProperties;

    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory() throws IOException {
        logger.info("Creating JPA EntityManagerFactory Bean..");
        LocalContainerEntityManagerFactoryBean emf = new LocalContainerEntityManagerFactoryBean();
        emf.setDataSource(dataSource);
        emf.setPackagesToScan(packagesToScan.split(","));
        emf.setPersistenceProvider(persistenceProvider);
        emf.setJpaProperties(jpaProperties.getObject());
        emf.setJpaVendorAdapter(jpaAdapter);
        return emf;
    }

}