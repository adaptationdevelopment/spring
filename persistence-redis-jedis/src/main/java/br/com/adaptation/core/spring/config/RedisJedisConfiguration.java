package br.com.adaptation.core.spring.config;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;

import redis.clients.jedis.JedisPoolConfig;

@Configuration
public class RedisJedisConfiguration {

	// Log4j
	private Logger logger = Logger.getLogger(RedisJedisConfiguration.class);

	@Value("${redis.host}")
	private String host;
	
	@Value("${redis.port:6379}")
	private Integer port;
	
	@Value("${redis.database:0}")
	private Integer database;
	
	@Value("${redis.timeout:3000}")
	private Integer timeout;

	@Value("${redis.password")
	private String password;
	
	@Value("${redis.pool.max_active:200}")
	private Integer maxActive;
	
	@Value("${redis.pool.max_idle:50}")
	private Integer maxIdle;
	
	@Value("${redis.pool.max_wait:3000}")
	private Integer maxWait;
	
	@Value("${redis.pool.test_on_borrow:true}")
	private Boolean testOnBorrow;
	
	@Bean
	public JedisPoolConfig jedisPoolConfig() {
		logger.info("Create Jedis Pool Config Bean..");	
		
		JedisPoolConfig pool = new JedisPoolConfig();
        pool.setMaxTotal(maxWait);
		pool.setMaxIdle(maxIdle);
		pool.setTestOnBorrow(testOnBorrow);
		return pool;
	}
	
	@Bean
	public RedisConnectionFactory redisConnectionFactory(JedisPoolConfig jedisPoolConfig) {
		logger.info("Create Jedis Connection Factory Bean..");	
		
		JedisConnectionFactory connFactory = new JedisConnectionFactory();
		connFactory.setHostName(host);
		connFactory.setPort(port);
		if(!database.toString().isEmpty()) connFactory.setDatabase(database);
		connFactory.setTimeout(timeout);
		//if(password.length()>0) connFactory.setPassword(password);
		connFactory.setUsePool(true);
		connFactory.setPoolConfig(jedisPoolConfig);
		return connFactory;
	}

}
