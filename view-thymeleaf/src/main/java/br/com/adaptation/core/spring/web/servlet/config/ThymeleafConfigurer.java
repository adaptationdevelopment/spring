package br.com.adaptation.core.spring.web.servlet.config;

import org.apache.log4j.Logger;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.web.servlet.config.annotation.ContentNegotiationConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
public class ThymeleafConfigurer extends WebMvcConfigurerAdapter {

	private Logger logger = Logger.getLogger(ThymeleafConfigurer.class);

	@Override
	public void configureContentNegotiation(
			ContentNegotiationConfigurer configurer) {

		logger.info("Setting Content Negotiation Configurer ..");

		configurer.defaultContentType(MediaType.TEXT_HTML).mediaType("html",
				MediaType.TEXT_HTML);
	}

}
