package br.com.adaptation.core.spring.beans.factory;

import java.util.HashSet;
import java.util.Set;

import nz.net.ultraq.thymeleaf.LayoutDialect;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.thymeleaf.dialect.IDialect;
import org.thymeleaf.spring4.SpringTemplateEngine;

@Configuration
public class ThymeleafDialectLayoutConfiguration implements InitializingBean {

	private Logger logger = Logger.getLogger(ThymeleafDialectLayoutConfiguration.class);
	
	@Autowired
	private SpringTemplateEngine springTemplateEngine;
	
	@Override
	public void afterPropertiesSet() throws Exception {
		
		logger.info("Adding LayoutDialect on SpringTemplateEngine Bean..");
		
		Set<IDialect> defaultDialects = springTemplateEngine.getDialects();
		
		Set<IDialect> dialects = new HashSet<IDialect>(defaultDialects);
		dialects.add(new LayoutDialect());
		
		springTemplateEngine.setDialects(dialects);
	}
	
}