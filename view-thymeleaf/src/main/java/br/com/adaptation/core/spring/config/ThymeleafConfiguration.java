package br.com.adaptation.core.spring.config;

import br.com.adaptation.core.spring.view.CustomThymeleafView;
import lombok.extern.log4j.Log4j;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.ViewResolver;
import org.thymeleaf.cache.StandardCacheManager;
import org.thymeleaf.dialect.IDialect;
import org.thymeleaf.extras.springsecurity3.dialect.SpringSecurityDialect;
import org.thymeleaf.spring4.SpringTemplateEngine;
import org.thymeleaf.spring4.dialect.SpringStandardDialect;
import org.thymeleaf.spring4.view.ThymeleafViewResolver;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;
import org.thymeleaf.templateresolver.FileTemplateResolver;
import org.thymeleaf.templateresolver.ServletContextTemplateResolver;
import org.thymeleaf.templateresolver.TemplateResolver;

import java.util.HashSet;
import java.util.Set;

@Configuration
@Log4j
public class ThymeleafConfiguration  {

	@Value("${thymeleaf.resolver.servlet_prefix:WEB-INF/thymeleaf/}")
	private String servletPrefix;

	@Value("${thymeleaf.resolver.classpath_prefix:thymeleaf/}")
	private String classpathPrefix;

	@Value("${thymeleaf.resolver.filesystem_prefix:/tmp}")
	private String filesystemPrefix;

	@Value("${thymeleaf.suffix:.html}")
	private String suffix;

	@Value("${thymeleaf.template_mode:HTML5}")
	private String templateMode;

	@Value("${thymeleaf.cacheable:true}")
	private Boolean cacheable;

	@Value("${thymeleaf.cache_ttlms:30}")
	private Long cacheTtlms;

	@Value("${thymeleaf.cache_max_size:30}")
	private Integer cacheMaxSize;
	
	@Bean
	public StandardCacheManager standardCacheManager() {
		
		log.info("Creating StandardCacheManager Bean..");
		
		StandardCacheManager cacheManager = new StandardCacheManager();
		cacheManager.setTemplateCacheMaxSize(cacheMaxSize);
		return cacheManager;
	}

	@Bean
	public ServletContextTemplateResolver servletContextTemplateResolver() {

		log.info("Creating ServletContextTemplateResolver Bean..");

		ServletContextTemplateResolver resolver = new ServletContextTemplateResolver();
		resolver.setPrefix(servletPrefix);
		resolver.setSuffix(suffix);
		resolver.setTemplateMode(templateMode);
		resolver.setCacheable(cacheable);
		resolver.setCacheTTLMs(cacheTtlms);
		resolver.setOrder(1);
		return resolver;
	}

	@Bean
	public ClassLoaderTemplateResolver classLoaderTemplateResolver() {

		log.info("Creating ClassLoaderTemplateResolver Bean..");

		ClassLoaderTemplateResolver resolver = new ClassLoaderTemplateResolver();
		resolver.setPrefix(classpathPrefix);
		resolver.setSuffix(suffix);
		resolver.setTemplateMode(templateMode);
		resolver.setCacheable(cacheable);
		resolver.setCacheTTLMs(cacheTtlms);
		resolver.setOrder(2);
		
		return resolver;
	}

	@Bean
	public FileTemplateResolver fileTemplateResolver() {

		log.info("Creating FileTemplateResolver Bean..");

		FileTemplateResolver resolver = new FileTemplateResolver();
		resolver.setPrefix(filesystemPrefix);
		resolver.setSuffix(suffix);
		resolver.setTemplateMode(templateMode);
		resolver.setCacheable(cacheable);
		resolver.setCacheTTLMs(cacheTtlms);
		resolver.setOrder(3);

		return resolver;
	}

	@Bean
	public SpringTemplateEngine springTemplateEngine(
			ServletContextTemplateResolver servletContextTemplateResolver,
			ClassLoaderTemplateResolver classLoaderTemplateResolver,
			FileTemplateResolver fileTemplateResolver,
			StandardCacheManager standardCacheManager) {

		log.info("Creating SpringTemplateEngine Bean..");
		
		Set<IDialect> dialects = new HashSet<IDialect>();
		dialects.add(new SpringStandardDialect());
		dialects.add(new SpringSecurityDialect());
		
		Set<TemplateResolver> templateResolvers = new HashSet<TemplateResolver>();
		templateResolvers.add(classLoaderTemplateResolver);
		templateResolvers.add(servletContextTemplateResolver);
		templateResolvers.add(fileTemplateResolver);

		SpringTemplateEngine templateEngine = new SpringTemplateEngine();
		templateEngine.setCacheManager(standardCacheManager);
		templateEngine.setTemplateResolvers(templateResolvers);
		templateEngine.setDialects(dialects);

		return templateEngine;
	}

	@Bean
	public ViewResolver thymeleafViewResolver(
			SpringTemplateEngine springTemplateEngine) {

		log.info("Creating ViewResolver Bean..");

		ThymeleafViewResolver viewResolver = new ThymeleafViewResolver();
		viewResolver.setTemplateEngine(springTemplateEngine);
		//viewResolver.addStaticVariable("wro4j", wroModelAccessor);
		viewResolver.setViewClass(CustomThymeleafView.class);
		return viewResolver;
	}

}
