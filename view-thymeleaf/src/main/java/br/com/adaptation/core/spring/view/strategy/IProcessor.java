package br.com.adaptation.core.spring.view.strategy;

public interface IProcessor {

	public String parse(String content);
	
}
