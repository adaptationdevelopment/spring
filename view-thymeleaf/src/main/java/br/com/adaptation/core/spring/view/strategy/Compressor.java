package br.com.adaptation.core.spring.view.strategy;

import com.googlecode.htmlcompressor.compressor.HtmlCompressor;

public class Compressor implements IProcessor{

	@Override
	public String parse(String content) {
		HtmlCompressor compressor = new HtmlCompressor();
		return compressor.compress(content);
	}

}
