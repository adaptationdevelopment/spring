package br.com.adaptation.core.spring.config;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.jolbox.bonecp.BoneCPDataSource;

@Configuration
public class DataSourceConfiguration {

	private Logger logger = Logger.getLogger(DataSourceConfiguration.class);

	@Value("${db.driver}")
	private String driver;
	
	@Value("${db.url}")
	private String url;
	
	@Value("${db.username}")
	private String username;
	
	@Value("${db.password}")
	private String password;
	
	@Value("${bonecp.idle_max_age_in_minutes}")
	private Long idleMaxAgeInMinutes;
	
	@Value("${bonecp.idle_connection_test_period_in_minutes}")
	private Long idleConnectionTestPeriodInMinutes;
	
	@Value("${bonecp.partition_count}")
	private Integer partitionCount;
	
	@Value("${bonecp.acquire_increment}")
	private Integer acquireIncrement;
	
	@Value("${bonecp.max_connections_per_partition}")
	private Integer maxConnectionsPerPartition;
	
	@Value("${bonecp.min_connections_per_partition}")
	private Integer minConnectionsPerPartition;
	
	@Value("${bonecp.statements_cache_size}")
	private Integer statementsCacheSize;
	
	@Bean
	public DataSource dataSource() {
		logger.info("Creating BoneCP DataSource Bean..");
		
		BoneCPDataSource ds = new BoneCPDataSource();
		ds.setDriverClass(driver);
		ds.setJdbcUrl(url);
		ds.setUsername(username);
		ds.setPassword(password);
		ds.setIdleMaxAgeInMinutes(idleMaxAgeInMinutes);
		ds.setIdleConnectionTestPeriodInMinutes(idleConnectionTestPeriodInMinutes);
		ds.setPartitionCount(partitionCount);
		ds.setAcquireIncrement(acquireIncrement);
		ds.setMaxConnectionsPerPartition(maxConnectionsPerPartition);
		ds.setMinConnectionsPerPartition(minConnectionsPerPartition);
		ds.setStatementsCacheSize(statementsCacheSize);
		return ds;
	}

}