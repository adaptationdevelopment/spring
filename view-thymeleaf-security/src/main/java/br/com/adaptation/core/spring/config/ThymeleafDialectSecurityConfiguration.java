package br.com.adaptation.core.spring.config;

import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.thymeleaf.dialect.IDialect;
import org.thymeleaf.extras.springsecurity3.dialect.SpringSecurityDialect;
import org.thymeleaf.spring4.SpringTemplateEngine;

import java.util.HashSet;
import java.util.Set;

@Configuration
@Log4j
public class ThymeleafDialectSecurityConfiguration implements InitializingBean {

    @Autowired
    private SpringTemplateEngine springTemplateEngine;

    @Override
    public void afterPropertiesSet() throws Exception {

        log.info("Adding SecurityDialect on SpringTemplateEngine Bean..");

        Set<IDialect> defaultDialects = springTemplateEngine.getDialects();

        Set<IDialect> dialects = new HashSet<IDialect>(defaultDialects);
        dialects.add(new SpringSecurityDialect());

        springTemplateEngine.setDialects(dialects);
    }

}
