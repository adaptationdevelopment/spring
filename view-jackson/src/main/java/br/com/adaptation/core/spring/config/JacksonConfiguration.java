package br.com.adaptation.core.spring.config;

import org.apache.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.servlet.ViewResolver;

import br.com.adaptation.core.spring.web.servlet.JacksonViewResolver;

@Configuration
public class JacksonConfiguration {

	private Logger logger = Logger.getLogger(JacksonConfiguration.class);
	
	@Bean
	public MappingJackson2HttpMessageConverter jsonHttpMessageConverter() {
		return new MappingJackson2HttpMessageConverter();
	}
	
	@Bean
	public ViewResolver jacksonViewResolver() {
		logger.info("Creating jackson ViewResolver Bean..");
		return new JacksonViewResolver();
	}
	
}