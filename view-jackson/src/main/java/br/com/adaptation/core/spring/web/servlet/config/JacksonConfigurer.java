package br.com.adaptation.core.spring.web.servlet.config;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.servlet.config.annotation.ContentNegotiationConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import java.util.List;

@Configuration
public class JacksonConfigurer extends WebMvcConfigurerAdapter {
	
	private Logger logger = Logger.getLogger(JacksonConfigurer.class);
	
	@Autowired
	MappingJackson2HttpMessageConverter jsonHttpMessageConverter;

	@Override
	public void configureContentNegotiation(
			ContentNegotiationConfigurer configurer) {

		logger.info("Setting Content Negotiation Configurer ..");

		configurer.mediaType("json", MediaType.APPLICATION_JSON);
	}

	@Override
    public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
        converters.add(jsonHttpMessageConverter);
        super.configureMessageConverters(converters);

    }

}
