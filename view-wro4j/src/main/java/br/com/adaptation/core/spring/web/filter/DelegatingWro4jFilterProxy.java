package br.com.adaptation.core.spring.web.filter;

import org.springframework.web.filter.DelegatingFilterProxy;

public class DelegatingWro4jFilterProxy extends DelegatingFilterProxy {

    public DelegatingWro4jFilterProxy() {
            super();
            setTargetFilterLifecycle(true);
    }
}