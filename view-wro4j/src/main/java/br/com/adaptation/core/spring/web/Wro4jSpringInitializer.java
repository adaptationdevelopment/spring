package br.com.adaptation.core.spring.web;

import javax.servlet.FilterRegistration;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;

import org.apache.log4j.Logger;
import org.springframework.web.WebApplicationInitializer;

import br.com.adaptation.core.spring.web.filter.DelegatingWro4jFilterProxy;

public class Wro4jSpringInitializer implements WebApplicationInitializer {

	// Log4j
	private Logger logger = Logger.getLogger(Wro4jSpringInitializer.class);

	@Override
	public void onStartup(ServletContext context) throws ServletException {

		logger.info("Starting Wro4j DelegatingWroFilterProxy..");
		
		FilterRegistration.Dynamic wroFilter = context
				.addFilter("wroFilter", DelegatingWro4jFilterProxy.class);
		
		wroFilter.addMappingForUrlPatterns(null, false, "/static/bundle/*");

	}
}