package br.com.adaptation.core.spring.config;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.config.PropertiesFactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import ro.isdc.wro.http.ConfigurableWroFilter;


@Configuration
public class Wro4jConfiguration {

	private Logger logger = Logger.getLogger(Wro4jConfiguration.class);
	
	@Bean
	public ConfigurableWroFilter wroFilter(PropertiesFactoryBean wroProperties) throws IOException {
		
		logger.info("Starting Wro4j ConfigurableWroFilter..");
		
		ConfigurableWroFilter filter = new ConfigurableWroFilter();
		filter.setProperties(wroProperties.getObject());
		return filter;
	}
	
	@Bean
	public PropertiesFactoryBean wroProperties() {
		
		logger.info("Starting Wro4j PropertiesFactoryBean..");
		
		PropertiesFactoryBean wroProperties =  new PropertiesFactoryBean();
		
		Resource[] resourceLocations = new Resource[] {
                new ClassPathResource("wro4j.properties")
        };
		
		wroProperties.setLocations(resourceLocations);
		wroProperties.setIgnoreResourceNotFound(true);
		return wroProperties;
	}
	
}