package br.com.adaptation.core.spring.config;

import org.apache.log4j.Logger;
import org.springframework.context.annotation.*;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;

@Configuration
@PropertySources({
        @PropertySource(value = "classpath://application.properties", ignoreResourceNotFound = true),
        @PropertySource("file://${appconfig.root}/application.properties")
})
@ComponentScan(basePackages = {"br.com.adaptation.core.spring", "${spring.base_packages}"})
public class ApplicationConfiguration {

    private static Logger logger = Logger.getLogger(ApplicationConfiguration.class);

    @Bean
    public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
        logger.info("Creating PropertySourcesPlaceholderConfigurer Bean..");
        return new PropertySourcesPlaceholderConfigurer();
    }

}