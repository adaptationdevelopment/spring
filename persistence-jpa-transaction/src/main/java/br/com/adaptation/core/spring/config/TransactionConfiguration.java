package br.com.adaptation.core.spring.config;

import org.apache.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;

@Configuration
@EnableTransactionManagement
public class TransactionConfiguration {

	// Log4j
	private Logger logger = Logger.getLogger(TransactionConfiguration.class);

	@Bean
	public PlatformTransactionManager transactionManager(EntityManagerFactory emf) {
		logger.info("Loading Transaction Manager..");
		
		return new JpaTransactionManager(emf);
	}

}