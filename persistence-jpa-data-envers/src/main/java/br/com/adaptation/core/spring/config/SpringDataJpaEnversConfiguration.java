package br.com.adaptation.core.spring.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.envers.repository.support.EnversRevisionRepositoryFactoryBean;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@EnableJpaRepositories(value = "${spring.base_packages}", repositoryFactoryBeanClass = EnversRevisionRepositoryFactoryBean.class)
public class SpringDataJpaEnversConfiguration {

}
