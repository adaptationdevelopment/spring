package br.com.adaptation.core.spring.config;

import br.com.adaptation.core.spring.annotation.ValidatorEventHandler;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.rest.core.event.ValidatingRepositoryEventListener;
import org.springframework.data.rest.webmvc.config.RepositoryRestMvcConfiguration;
import org.springframework.validation.Validator;

import java.util.Iterator;
import java.util.Map;


@Configuration
@Log4j
public class CustomRepositoryRestMvcConfiguration extends RepositoryRestMvcConfiguration {

    @Autowired
    ApplicationContext appContext;

    @Override
	protected void configureValidatingRepositoryEventListener(ValidatingRepositoryEventListener v) {

        Map<String, Object> validators = appContext
                .getBeansWithAnnotation(ValidatorEventHandler.class);

        if (!validators.isEmpty()) {

            Iterator<Map.Entry<String, Object>> iterator = validators.entrySet()
                    .iterator();

            while (iterator.hasNext()) {

                Map.Entry mapEntry = (Map.Entry) iterator.next();
                Object object = mapEntry.getValue();

                if (object instanceof Validator) {

                    log.info("Add ValidatorEventHandler [ "
                            + object.getClass() + " ]..");

                    ValidatorEventHandler validatorEventHandler = object.getClass().getAnnotation(ValidatorEventHandler.class);

                    String value = validatorEventHandler.value();
                    if (!value.isEmpty()) {
                        Validator validator = (Validator) object;
                        v.addValidator(value, validator);
                    }
                }

            }
        }

	}

	
}