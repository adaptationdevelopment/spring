package br.com.adaptation.core.spring.config;

import org.apache.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.xml.MarshallingHttpMessageConverter;
import org.springframework.oxm.castor.CastorMarshaller;
import org.springframework.web.servlet.ViewResolver;

import br.com.adaptation.core.spring.web.servlet.CastorViewResolver;

@Configuration
public class CastorConfiguration {

	private Logger logger = Logger.getLogger(CastorConfiguration.class);

	@Bean
	public CastorMarshaller castorMarshaller() {
		return new CastorMarshaller();
	}
	
	@Bean
	public MarshallingHttpMessageConverter marshallingHttpMessageConverter(
			CastorMarshaller marshaller) {
		MarshallingHttpMessageConverter converter = new MarshallingHttpMessageConverter();
		converter.setMarshaller(marshaller);
		converter.setUnmarshaller(marshaller);
		return converter;
	}

	@Bean
	public ViewResolver marshallingViewResolver(
			CastorMarshaller marshaller) {
		logger.info("Creating marshalling ViewResolver Bean..");
		return new CastorViewResolver(marshaller);
	}

}
