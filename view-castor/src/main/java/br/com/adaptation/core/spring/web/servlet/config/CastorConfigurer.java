package br.com.adaptation.core.spring.web.servlet.config;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.xml.MarshallingHttpMessageConverter;
import org.springframework.web.servlet.config.annotation.ContentNegotiationConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
public class CastorConfigurer extends WebMvcConfigurerAdapter {

	private Logger logger = Logger.getLogger(CastorConfigurer.class);

	@Autowired
	private MarshallingHttpMessageConverter marshallingHttpMessageConverter;
	
	@Override
	public void configureContentNegotiation(
			ContentNegotiationConfigurer configurer) {

		logger.info("Setting Content Negotiation Configurer ..");
		configurer.mediaType("xml", MediaType.APPLICATION_XML);
	}
	
	@Override
    public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
        converters.add(marshallingHttpMessageConverter);
        super.configureMessageConverters(converters);

    }
	
}