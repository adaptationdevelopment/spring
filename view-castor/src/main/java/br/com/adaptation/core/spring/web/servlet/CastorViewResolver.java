package br.com.adaptation.core.spring.web.servlet;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.oxm.Marshaller;
import org.springframework.web.servlet.View;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.view.xml.MarshallingView;

public class CastorViewResolver implements ViewResolver {

	private Marshaller marshaller;

	@Autowired
	public CastorViewResolver(Marshaller marshaller) {
		this.marshaller = marshaller;
	}

	@Override
	public View resolveViewName(String viewName, Locale locale)
			throws Exception {
		MarshallingView view = new MarshallingView();
		view.setMarshaller(marshaller);
		view.setContentType(MediaType.APPLICATION_XML_VALUE);
		return view;
	}

}
