package br.com.adaptation.core.spring.config;

public interface CacheKeyGenerator {

	String getKey();
	
}