package br.com.adaptation.core.spring.config;

import java.lang.reflect.Method;
import java.util.List;

import lombok.Builder;
import lombok.Setter;
import lombok.extern.log4j.Log4j;

import org.springframework.cache.interceptor.KeyGenerator;

@Log4j
@Builder
public class CustomKeyGenerator implements KeyGenerator{

	@Setter
	private List<CacheKeyGenerator> generators;
	
	@Setter
	private String separator;
	
	@Override
	public Object generate(Object target, Method method, Object... params) {
		StringBuilder key = new StringBuilder();

		for (Object param : params) {
			key.append(appendSeparator(param.toString()));
		}
		
		for (CacheKeyGenerator generator : generators) {

			CacheKey cacheKey = generator.getClass().getAnnotation(
					CacheKey.class);
			String keyFromGenerator = appendSeparator(generator.getKey());
			
			Boolean after = cacheKey.afterParams();
			if (after) {
				key.append(keyFromGenerator);
			} else {
				key.insert(0, keyFromGenerator);
			}
		}
		
		log.debug("Cache Key [ " + key.toString() + " ] generated..");
		
		return key.toString();
	}
	
	private String appendSeparator(String key) {
		if (!separator.isEmpty()) {
			key = key + separator;
		}
		return key;
	}
	
	/*
	private String parseParam(Object param) throws ClassNotFoundException {
		
		Class<?> cls = Class.forName(param.getClass().toString());
		if (cls.isAnnotationPresent())
		
		return null;
	}
	*/

}
