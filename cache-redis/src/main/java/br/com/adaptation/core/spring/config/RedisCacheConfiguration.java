package br.com.adaptation.core.spring.config;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import lombok.Setter;
import lombok.extern.log4j.Log4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CachingConfigurer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.interceptor.CacheErrorHandler;
import org.springframework.cache.interceptor.CacheResolver;
import org.springframework.cache.interceptor.KeyGenerator;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.cache.RedisCacheManager;
import org.springframework.data.redis.core.RedisTemplate;

@Configuration
@EnableCaching
@Log4j
public class RedisCacheConfiguration implements CachingConfigurer {

	@Value("${spring.cache.key.separator:.}")
	private String separator;

	@Autowired
	@Setter
	RedisTemplate redisTemplate;

	@Autowired
	@Setter
	ApplicationContext context;

	@Bean
	public CacheManager cacheManager() {
		log.info("Creating Redis Cache Manager Bean..");
		return new RedisCacheManager(redisTemplate);
	}

    @Override
    public CacheResolver cacheResolver() {
        return null;
    }

    @Bean
	public KeyGenerator keyGenerator() {
		return CustomKeyGenerator.builder().generators(getGenerators())
				.separator(separator).build();
	}

    @Override
    public CacheErrorHandler errorHandler() {
        return null;
    }

    private List<CacheKeyGenerator> getGenerators() {

		List<CacheKeyGenerator> generators = new ArrayList<CacheKeyGenerator>();

		Map<String, Object> generatorsScanned = context
				.getBeansWithAnnotation(CacheKey.class);

		if (!generatorsScanned.isEmpty()) {
			Iterator<Entry<String, Object>> iterator = generatorsScanned
					.entrySet().iterator();

			while (iterator.hasNext()) {

				Map.Entry mapEntry = (Map.Entry) iterator.next();
				Object object = mapEntry.getValue();

				if (object instanceof CacheKeyGenerator) {

					log.info("Add Cache Key Generator [ " + object.getClass()
							+ " ]..");

					CacheKeyGenerator generator = (CacheKeyGenerator) object;

					generators.add(generator);

				}

			}
		}

		return generators;

	}

}