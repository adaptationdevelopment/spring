package br.com.adaptation.core.spring.audit.aware;

import br.com.adaptation.core.spring.audit.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.AuditorAware;
import org.springframework.stereotype.Component;

@Component
public class AuditorAwareImpl implements AuditorAware<String> {

    @Autowired
    private UserService userService;

    @Override
    public String getCurrentAuditor() {
        return userService.getCurrentUser();
    }

}