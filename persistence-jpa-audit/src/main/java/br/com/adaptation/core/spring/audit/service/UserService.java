package br.com.adaptation.core.spring.audit.service;


public interface UserService {

    String getCurrentUser();

}
