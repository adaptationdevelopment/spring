package br.com.adaptation.core.spring.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@EnableJpaRepositories("${spring.base_packages}")
public class SpringDataJpaConfiguration {

}
