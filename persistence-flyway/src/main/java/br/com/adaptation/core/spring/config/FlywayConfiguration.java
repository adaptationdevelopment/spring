package br.com.adaptation.core.spring.config;

import lombok.extern.log4j.Log4j;
import org.flywaydb.core.Flyway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

@Configuration
@Log4j
public class FlywayConfiguration {

    @Value("${flyway.locations}")
    private String locations;

    @Value("${flyway.table:schema_version}")
    private String table;

    @Autowired
    private DataSource dataSource;

    @Bean(initMethod = "migrate")
    public Flyway flyway() {

        log.info("Creating Flyway Bean..");

        Flyway flyway = new Flyway();
        flyway.setBaselineOnMigrate(true);
        flyway.setLocations(locations);
        flyway.setDataSource(dataSource);
        flyway.setTable(table);
        return flyway;
    }


}
