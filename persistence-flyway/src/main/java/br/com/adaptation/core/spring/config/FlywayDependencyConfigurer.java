package br.com.adaptation.core.spring.config;

import lombok.extern.log4j.Log4j;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.context.annotation.Configuration;

@Configuration
@Log4j
public class FlywayDependencyConfigurer implements BeanFactoryPostProcessor {

    @Override
    public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {
        log.info("Configure entityManagerFactory to depends the flyway bean");
        BeanDefinition bd = beanFactory.getBeanDefinition("entityManagerFactory");
        String[] beans = {"flyway"};
        bd.setDependsOn(beans);
    }

}
